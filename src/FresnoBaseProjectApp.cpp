
#include "FresnoBaseProjectApp.h"


FresnoBaseProjectApp::FresnoBaseProjectApp() {
	effectID = 51;
}

void FresnoBaseProjectApp::InitDevMode() {
	myTexture = loadImage(loadAsset( "dot.png" ));

	//TODO
	setCanvasSizeVars(getWindowSize().x, getWindowSize().y);
}

void FresnoBaseProjectApp::setCanvasSizeVars(float w, float h) {
	canvasWidth = w;
	canvasHeight = h;
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;
}

//should only occur in dev (standalone) mode
void FresnoBaseProjectApp::setup()
{
#ifdef DEV_MODE
	InitDevMode();
	globalSetup();
#endif

}

void FresnoBaseProjectApp::mouseDown( MouseEvent event )
{
}

void FresnoBaseProjectApp::update()
{
	Update();
}

void FresnoBaseProjectApp::draw()
{
	Draw();
}

void FresnoBaseProjectApp::Update() {
	counter++;
	redVal = sin(counter * 0.01f);
	redVal = lmap(redVal, -1.0f, 1.0f, 0.0f, 1.0f);
	blueVal = sin(counter * 0.013f);
	redVal = lmap(blueVal, -1.0f, 1.0f, 0.0f, 1.0f);
	greenVal = sin(counter * 0.017f);
	redVal = lmap(greenVal, -1.0f, 1.0f, 0.0f, 1.0f);

	pe.update();
}
void FresnoBaseProjectApp::Draw() {
	isLocked = true;

#ifdef DEV_MODE
	gl::pushMatrices();
	gl::translate(canvasHalfWidth, canvasHalfHeight); 
#endif

	//gl::clear( Color( redVal, blueVal, greenVal ) ); 
	gl::clear( Color( 0, 0, 0 ) ); 

	gl::enableAlphaBlending();
	//gl::draw( myTexture, Rectf(-50, -50, 50, 50) );
	pe.render();
	gl::disableAlphaBlending();

#ifdef DEV_MODE
	gl::popMatrices();
#endif

	isLocked = false;
}

void FresnoBaseProjectApp::globalSetup() {
	pe.setup(myTexture);
	counter = 0;

	int numParticles = 10;
	for (int i = 0; i < numParticles; i++) {
		Particle p(canvasWidth, canvasHeight);
		pe.add(p);
	}
}

void FresnoBaseProjectApp::Initialize(char* InitData, Timeline* CinderTimeline) {

	// skip two characters
	int i = 2, j = 0;
	while(InitData[i++] != ';') {}
	j = i;
	
	char ParentWidth[8], ParentHeight[8];	

	for(i; i < (j + 6); i++) {
		ParentWidth[i - j] = InitData[i];
	}
	ParentWidth[6] = '\0';
	j = i;

	for(i; i < (j + 6); i++) {
		ParentHeight[i - j] = InitData[i];
	}
	ParentHeight[6] = '\0';
	j = i;

	setCanvasSizeVars(atoi(ParentWidth), atoi(ParentHeight));
	//myTexture = gl::Texture(loadImage("DefaultAssets/00051/dot.png"));
	// create a directory in program files directory under Default Assets...
	// paste your texture file in the folder and continue from that

	//setCanvasSizeVars(atoi(ParentWidth), atoi(ParentHeight));
	setCanvasSizeVars(1920, 1080);
	myTexture = gl::Texture(loadImage("DefaultAssets/images/dot.png"));

	globalSetup();
}
void FresnoBaseProjectApp::SetEffectData(char* Data) {
}

Vec2f getWorldCoords(Vec2f normalizedCoords) {
	Vec2f worldCoords;
#ifdef DEV_MODE

#else
#endif


	return worldCoords;
}

#ifdef DEV_MODE
CINDER_APP_NATIVE( FresnoBaseProjectApp, RendererGl )
#else
extern "C"{
	__declspec(dllexport) IEffectInterface* GetInterface(){
		IEffectInterface * effect = new FresnoBaseProjectApp();
		return effect;
		}}
#endif