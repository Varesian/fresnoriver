#include "Particle.h"

Particle::Particle(float canvasWidth_, float canvasHeight_) : canvasWidth(canvasWidth_), canvasHeight(canvasHeight_) {
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;

	reset();

	//initially set some way back to stagger entrance
	position.x = Rand::randFloat(-canvasWidth, -canvasHalfWidth) - size;
}

Vec2f Particle::getPosition() {
	return position;
}

float Particle::getSize() {
	return size;
}

ColorA Particle::getColor() {
	return col;
}


void Particle::update() {
	position += normDirection * speed;

	if (position.x > canvasHalfWidth + size) {
		isDead = true;
	}

	if (isDead) {
		reset();
	}
}

void Particle::reset() {
	size = Rand::randFloat(0.05f, 0.1f);
	size *= canvasWidth;
	float x = -size - canvasHalfWidth;
	float y = Rand::randFloat(-1.0f, 1.0f);
	y *= canvasHalfHeight; 
	position = Vec2f(x, y);
	normDirection = Vec2f(1.0f, 0);
	float tempSpeedScalar = 1.0f;
	speed = Rand::randFloat(0.001f, 0.002f);
	speed *= tempSpeedScalar;
	speed *= canvasWidth;
	col = ColorA(Rand::randFloat(0.25f, 0.9f), 1.0f, 1.0f, 0.5f);
	isDead = false;
}
