#include "ParticleEmitter.h"

void ParticleEmitter::setup(cinder::gl::Texture tex) {
	particleTexture = tex;
}

void ParticleEmitter::add(Particle p) {
	particles.push_back(p);
}

void ParticleEmitter::render() {
	gl::enableAlphaBlending();
	particleTexture.enableAndBind();
	int size = particles.size();
	for (int i = 0; i < size; i++) {
		gl::color( particles[i].getColor() );
		float partSize = particles[i].getSize();
		ci::gl::drawSolidRect(ci::Rectf(
			particles[i].getPosition().x - partSize,
			particles[i].getPosition().y - partSize,
			particles[i].getPosition().x + partSize,
			particles[i].getPosition().y + partSize));
	}
	gl::disableAlphaBlending();
}

void ParticleEmitter::update() {
	int size = particles.size();
	for (int i = 0; i < size; i++) {
		particles[i].update();
	}
}